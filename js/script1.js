var xmlhttp = new XMLHttpRequest();
var limit = 3;
var initLimit = 6;
var newArr = [];
var newData=[];
var arrLength = 0;
xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        const data = JSON.parse(this.responseText);
        data.forEach((item) => {
            item['titleLowercase'] = item.title.toLowerCase();
        });
        arrLength = data.length;
        newData = data.slice();
        newArr = newData.slice(0,initLimit);
        arrHandler(newArr);
        searchFilter(newArr);
        categoryFilter(data);
        if (data.length < 0) {
            noResult('false');
        }
    }
};
xmlhttp.open("GET", "dummy.json", true);
xmlhttp.send();

/* Array Variables */
arrHandler = (newArr)=>{
    var categoryName = [];
    var categorySlug = [];
    var categoryObj = {};
    var categoryNameUnique = [];
    var categorySlugUnique = [];
    var category = [];
    newArr.forEach((item) => {
        const id = item.id;
        const title = item.title;
        const body = item.paragraph;
        const category_name = item.category_name;
        const category_slug = item.category_slug;
        categoryName.push(category_name);
        categorySlug.push(category_slug);
        categoryNameUnique = [...new Set(categoryName)];
        categorySlugUnique = [...new Set(categorySlug)];
        htmlHandler(id, title, body,category_name);
    });
    for (var i = 0; i < categoryNameUnique.length; i++) {
        categoryObj = {
            category_name: categoryNameUnique[i],
            category_slug: categorySlugUnique[i]
        };
        category.push(categoryObj);
    }
    category.forEach((item) => {
        const category_name = item.category_name;
        const category_slug = item.category_slug;
        categoryHandler(category_name, category_slug);
    });
}


/* Add item html into DOM */
const htmlHandler = (id, title, body,category_name) => {
    const parentHandler = document.querySelector(".posts-grid .grid-block");
    const itemHtml = '<div class="block"><div class="inner-wrap wow wefadeInUp"><div class="content-wrap"><h6>' + id + '</h6><h6>' + title + '</h6><p>' + body + '</p><span>' + category_name + '</span></div></div></div>';
    parentHandler.insertAdjacentHTML("beforeend", itemHtml);
}

/* Add category to DOM */
const categoryHandler = (category_name, category_slug) => {
    const parentHandler = document.querySelector(".posts-grid .block-wrap .select ul");
    const itemHtml = '<li data-slug="' + category_slug + '">' + category_name + '</li>';
    parentHandler.insertAdjacentHTML("beforeend", itemHtml);
}

/* No result found */
const noResult = (result) => {
    const noResult = document.querySelector('.no-record');
    if (result === "false") {
        noResult.style['display'] = 'block';
    } else {
        noResult.style['display'] = 'none';
    }
}

/* Array for filtered values */
var searchResult = [];
const list = document.querySelector(".posts-grid .block-wrap .select ul");
/* Search filter */
const searchFilter = (data) => {
    const searchValue = document.getElementById('searchText');
    // searchValue.addEventListener("keyup", () => {
    //     const categoryValue = document.querySelectorAll('.posts-grid .block-wrap .select span');
    //     console.log(categoryValue);
    //     const categorySlug = categoryValue.getAttribute('data-slug');
        const newValue = searchValue.value.toLowerCase();
        //newData = data.filter(filterItem => filterItem.category_slug.includes(slug));
        newData = newData.filter(item => item.titleLowercase.includes(newValue));
        newArr = newData.slice(0,initLimit);
        filterHandler(newArr);
    });
}

/* Category filter */
const categoryFilter = (data) => {
    const categoryValue = document.querySelectorAll('.posts-grid .block-wrap .select ul li');
    for (i = 0; i < categoryValue.length; i++) {
        const slug = categoryValue[i].getAttribute('data-slug');
        categoryValue[i].addEventListener("click", () => {
            list.classList.remove('active');
            // const searchValue = document.getElementById('searchText');
            // const newValue = searchValue.value.toLowerCase();
            // newData = data.filter(item => item.titleLowercase.includes(newValue));
            newData = newData.filter(filterItem => filterItem.category_slug.includes(slug));
            newArr = newData.slice(0,initLimit);
            filterHandler(newArr);
        });
    }
}

/* filterHandler */
filterHandler = (newArr) => {
    const parentHandler = document.querySelector(".posts-grid .grid-block");
    parentHandler.innerHTML = '';
    if (newArr.length > 0) {
        newArr.forEach((item) => {
            const id = item.id;
            const title = item.title;
            const body = item.paragraph;
            const category_name = item.category_name;
            htmlHandler(id, title, body,category_name);
        });
        noResult('true');
    } else {
        noResult('false');
    }
}
/* Dropdown opne/close */
const dropDownOpen = document.querySelector('.span-border');
dropdown = ()=>{
    
    if(list.classList.contains("active")){
        list.classList.remove('active');
    }else{
        list.classList.add('active');
    }
}
dropDownOpen.addEventListener("click",dropdown);

/* Load more button */
const button = document.getElementById('load-more');
function loadmoreHandler(){
    const newLimit = initLimit + limit;
    newArr = newData.slice(initLimit,newLimit);
    initLimit += limit;
    arrHandler(newArr);
    if((arrLength - newLimit) < limit){
        button.style.display='none';
    }
}

button.addEventListener("click",loadmoreHandler);